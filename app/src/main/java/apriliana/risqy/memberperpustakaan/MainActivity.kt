package apriliana.risqy.memberperpustakaan

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import apriliana.risqy.memberperpustakaan.DaftarActivity
import apriliana.risqy.memberperpustakaan.LoginActivity
import apriliana.risqy.memberperpustakaan.UploadActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), View.OnClickListener {

    var fbAuth = FirebaseAuth.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnDaftar.setOnClickListener(this)

        btnLogoff.setOnClickListener{
            fbAuth.signOut()
            Toast.makeText(this, "Berhasil Log Out", Toast.LENGTH_SHORT).show()
            var intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }
        btnUp.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnDaftar ->{
                var intent = Intent(this, DaftarActivity::class.java)
                startActivity(intent)
            }
            R.id.btnUp ->{
                var intent = Intent(this, UploadActivity::class.java)
                startActivity(intent)
            }
        }
    }

}
