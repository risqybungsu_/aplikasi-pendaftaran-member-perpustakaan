package apriliana.risqy.memberperpustakaan

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.SimpleAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import apriliana.risqy.memberperpustakaan.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_daftar.*

class DaftarActivity : AppCompatActivity(), View.OnClickListener {

    val COLLECTION = "member"
    val F_ID = "id"
    val F_NAME = "nama"
    val F_ADDRESS = "alamat"
    val F_NIK = "nik"
    var docId = ""
    var fbAuth = FirebaseAuth.getInstance()
    lateinit var db : FirebaseFirestore
    lateinit var alMember : ArrayList<HashMap<String,Any>>
    lateinit var adapter: SimpleAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_daftar)

        alMember = ArrayList()
        btnDaftar.setOnClickListener(this)
        btnLogoff.setOnClickListener{
            fbAuth.signOut()
            Toast.makeText(this, "Berhasil Log Out", Toast.LENGTH_SHORT).show()
            var intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }
        btnUp.setOnClickListener(this)
        lsData.setOnItemClickListener(itemClick)
    }

    override fun onStart() {
        super.onStart()
        db = FirebaseFirestore.getInstance()
    }
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnDaftar ->{
                val hm = HashMap<String, Any>()
                hm.set(F_ID,edid.text.toString())
                hm.set(F_NAME,edName.text.toString())
                hm.set(F_ADDRESS,edAddress.text.toString())
                hm.set(F_NIK,edPhone.text.toString())

                db.collection(COLLECTION).document(edid.text.toString()).set(hm).
                addOnSuccessListener {
                    Toast.makeText(this,"Data Succesfully Added", Toast.LENGTH_SHORT)
                        .show()
                }.addOnFailureListener{ e ->
                    Toast.makeText(this,
                        "Data Unsuccesfully Added : ${e.message}", Toast.LENGTH_SHORT)
                        .show()
                }

            }
        }
    }

    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val hm = alMember.get(position)
        docId = hm.get(F_ID).toString()
        edid.setText(docId)
        edName.setText(hm.get(F_NAME).toString())
        edAddress.setText(hm.get(F_ADDRESS).toString())
        edPhone.setText(hm.get(F_NIK).toString())

    }
    fun showData(){
        db.collection(COLLECTION).get().addOnSuccessListener { result ->
            alMember.clear()
            for(doc in result){
                val hm = HashMap<String,Any>()
                hm.set(F_ID,doc.get(F_ID).toString())
                hm.set(F_NAME,doc.get(F_NAME).toString())
                hm.set(F_ADDRESS,doc.get(F_ADDRESS).toString())
                hm.set(F_NIK,doc.get(F_NIK).toString())
                alMember.add(hm)
            }
        }
    }

}